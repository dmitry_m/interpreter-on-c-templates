#pragma once 

#include <iostream>
#include <string>
#include <memory>
#include <functional>
#include <vector>

#include "arithm.h"
#include "printer.h"
#include "types.h"
#include "traits.h"

template <int Offset, int Took>
struct NextArgOffset {
	static const int get = Offset;
};

template <int Offset>
struct NextArgOffset<Offset, 0> {
	static const int get = Offset + 1;
};

struct ParentRoot;

// gather argument
// arguments order is inverted!
template <class Parent, class Function, int Takes, int Took, int Pos, class... Args>
struct CalculateImpl {
	static const int nextArgOffsetBaseOffset = NextArgOffset<Pos, Took>::get;

	using nextArgType = typename Get<nextArgOffsetBaseOffset, Args...>::type;
	using nextArgCalculation = CalculateImpl <Function, nextArgType, nextArgType::takes, 0, nextArgOffsetBaseOffset, Args... >;

	static const int arg = nextArgCalculation::result;
	static const int nextArgPos = nextArgCalculation::nextPos;
	using nextCalculation = CalculateImpl <Parent, Function, Takes, Took + 1, nextArgPos + 1, SaveArg<Took, Number<arg>>, Args... >;

	static const int nextPos = nextCalculation::nextPos;
	static const int result = nextCalculation::result;
};

// ref handling
template <class Parent, class Function, int Took, int Pos, class... Args>
struct CalculateImpl<Parent, Function, REFERENCE_ID, Took, Pos, Args...> {
	static const int nextPos = Pos + 1;
	static const int result = GetArgByIndex<Function::index, Args...>::type::result;
};

// number handling
template <class Parent, class Function, int Took, int Pos, class... Args>
struct CalculateImpl<Parent, Function, NUMBER_ID, Took, Pos, Args...> {
	static const int nextPos = Pos + 1;
	static const int result = Get<Pos, Args...>::type::result;
};

// forbidden to be calculated
template <class Parent, class Function, int Takes, int Pos, class... Args>
struct CalculateImpl<Parent, Function, FORBIDDEN_TO_BE_CALCULATED_ID, Takes, Pos, Args...> {
	static const int nextPos = Pos - Takes;
	static const int result = 0;
};

// all arguments gathered
template <class Parent, class Function, int Takes, int Pos, class... Args>
struct CalculateImpl<Parent, Function, Takes, Takes, Pos, Args...> {
	static const int nextPos = Pos - Takes;
	static const int result = CalculateExpr<Function, Args...>::result;
};

template <class Head, class... Args>
struct Calculate : public Printable<Head, Args...> {
	using calculation = CalculateImpl < ParentRoot, Head, Head::takes, 0, 0, Head, Args... >;

	static const int takes = -2;
	static const int result = calculation::result;
};

#include "calculator_defines.h"