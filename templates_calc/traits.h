#pragma once

#include "types.h"

template <class Arg>
struct isFunc {
	static const int yes = (0 <= Arg::takes);
};

template <class Arg>
struct isNum {
	static const int yes = (NUMBER_ID == Arg::takes);
};

template <class Obj>
struct isRef {
	static const int yes = (REFERENCE_ID == Obj::takes);
};

template <class Arg>
struct isArg {
	static const int yes = (ARGUMENT_ID == Arg::takes);
};

template <class Arg>
struct isForbiddenToBeCalculated {
	static const int yes = (FORBIDDEN_TO_BE_CALCULATED_ID == Arg::takes);
};

// fails if condition is false
template <int>
struct CheckThat {
	using perform = void;
};

template <>
struct CheckThat<0> {
};

// fails if condition is true
template <int>
struct CheckThatNo {
	using perform = void;
};

template <>
struct CheckThatNo<1> {
};

template <template <class> class Pred, class Head, class... Args>
struct CheckRecursively {
	using perform = typename CheckThatNo<Pred<Head>::yes>::perform;
	using next = typename CheckRecursively<Pred, Args...>::perform;
};

template <template <class> class Pred, class... Args>
struct CheckRecursively<Pred, Args...> {
	using perform = void;
};

template <int skipFirst, class Head, class... Args>
struct CheckForForbidden {
	using perform = typename CheckRecursively<isForbiddenToBeCalculated, Args...>::perform;
};

template <class Head, class... Args>
struct CheckForForbidden<0, Head, Args...> {
	using perform = typename CheckRecursively<isForbiddenToBeCalculated, Head, Args...>::perform;
};