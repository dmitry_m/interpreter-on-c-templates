#pragma once

#include "get.h"


// common

#define DEFINE_TAG(name, arguments)\
	struct name { static const int takes = arguments; };

#define LINK_TAG_TO_OPERATION(name)\
	template <class... Args>\
	struct CalculateExpr < name, Args... > {\
		static const int result = name##Impl < Args... >::result; \
	};

#define LINK_TAG_TO_OPERATION_HANDLER(name, handler)\
	template <class... Args>\
	struct CalculateExpr < name, Args... > {\
		static const int result = handler < name##Impl, Args... >::result; \
	};

#define DEFINE_OPERATION_IMPL(name, ...)\
	template <class... Args>\
	struct name##Impl {\
		using nextCalculation = Calculate<__VA_ARGS__, Args...>; \
		static const int result = nextCalculation::result; \
	};

#define define(name, arguments, ...)\
	DEFINE_TAG(name, arguments)\
	DEFINE_OPERATION_IMPL(name, __VA_ARGS__)\
	LINK_TAG_TO_OPERATION(name)
	

// unary specific

#define LINK_UNARY_TAG_TO_OPERATION(name) \
	LINK_TAG_TO_OPERATION_HANDLER(name,UnaryOperation)

#define DEFINE_UNARY_OPERATION_IMPL(name, operation)\
	template <class Arg>\
	struct name##Impl {\
		static const int result = operation Arg::result;\
		};

#define DEFINE_UNARY_OPERATION(name, operation)\
	DEFINE_TAG(name, 1)\
	DEFINE_UNARY_OPERATION_IMPL(name, operation)\
	LINK_UNARY_TAG_TO_OPERATION(name)


// binary specific

#define LINK_BINARY_TAG_TO_OPERATION(name) \
	LINK_TAG_TO_OPERATION_HANDLER(name, BinaryOperation)

#define DEFINE_BINARY_OPERATION_IMPL(name, operation)\
	template <class Lhs, class Rhs>\
	struct name##Impl {\
		static const int result = Lhs::result operation Rhs::result;\
	};

#define DEFINE_BINARY_OPERATION(name, operation)\
	DEFINE_TAG(name, 2)\
	DEFINE_BINARY_OPERATION_IMPL(name, operation)\
	LINK_BINARY_TAG_TO_OPERATION(name)



template <template <class> class Operation, class... Args>
struct UnaryOperation;

template <template <class> class Operation, class First, class... Args>
struct UnaryOperation<Operation, First, Args...> {
	static const int result = Operation<First::type>::result;
};

template <template <class, class> class Operation, class... Args>
struct BinaryOperation;

template <template <class, class> class Operation, class First, class Second, class... Args>
struct BinaryOperation<Operation, First, Second, Args...> {
	static const int result = Operation<Second::type, First::type>::result;
};

template <class Function, class... Args>
struct CalculateExpr;

DEFINE_UNARY_OPERATION(minus, -)
DEFINE_BINARY_OPERATION(sum, +)
DEFINE_BINARY_OPERATION(sub, -)
DEFINE_BINARY_OPERATION(mul, *)
DEFINE_BINARY_OPERATION(divi, /)
