#pragma once

#include <iostream>
#include "arithm.h"
#include "types.h"

DEFINE_TAG(print, FORBIDDEN_TO_BE_CALCULATED_ID)

template <class... Args>
struct printRecursive {
	static void print() {}
};

template <class Head, class... Args>
struct printRecursive<Head, Args...> {
	static void print() {
		std::cout << Head::result;
		printRecursive<Args...>::printRecursive();
	}
};

template <class... Args>
struct Printable {};

template <class... Args>
struct Printable<print, Args...> {
	static void run() {
		printRecursive<Args...>::print();
	}
};