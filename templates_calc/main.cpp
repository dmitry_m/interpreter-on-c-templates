#include "calculator.h"

define(increment, 1, sum with arg<0> and num<1>)

begin
	prints "Hi!" 

	newline 
	newline

	prints "Increment of 2 = " end

	call print with 
		call 
			increment with
				num < 2 >
			now
		now perform

	newline
	newline
	
	prints "And (10 - 1) * (24 / 3) + 1 = " end
	call print with
		call sum with
			mul with
				sub with
					num<10>,
					num<1>,
				divi with
					num<24>,
					num<3>,
			num<1>
			now 
		now perform	
endprog
