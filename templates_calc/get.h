#pragma once

#include "traits.h"

template <int index, class... Args>
struct Get;

template <class Head, class... Args>
struct Get<0, Head, Args...> {
	using type = Head;
};

template <int index, class Head, class... Args>
struct Get<index, Head, Args...> {
	using type = typename Get<index - 1, Args...>::type;
};


template <int index, class... Args>
struct GetArgByIndex;


template <int index, int argIndex, class... Args>
struct LookForIndex;

template <int index, class Head, class... Args>
struct LookForIndex<index, index, Head, Args...> {
	using type = typename Head::type;
};

template <int index, int argIndex, class Head, class... Args>
struct LookForIndex<index, argIndex, Head, Args...> {
	using type = typename GetArgByIndex<index, Args...>::type;
};

template <int index, int isArg, class Head, class... Args>
struct LookForArg {
	using type = typename LookForIndex < index, Head::index, Head, Args... >::type;
};

template <int index, class Head, class... Args>
struct LookForArg<index, 0, Head, Args...> {
	using type = typename GetArgByIndex<index, Args...>::type;
};


template <int index, class Head, class... Args>
struct GetArgByIndex<index, Head, Args...> {
	using type = typename LookForArg<index, isArg<Head>::yes, Head, Args...>::type;
};