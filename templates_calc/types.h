#pragma once 

#define REFERENCE_ID -1
#define NUMBER_ID -2
#define ARGUMENT_ID -9
#define FORBIDDEN_TO_BE_CALCULATED_ID -10

template <int value>
struct Reference {
	static const int takes = REFERENCE_ID;
	static const int index = value;
};

template <int value>
struct Number {
	static const int takes = NUMBER_ID;
	static const int result = value;
};

template <int ind, class Value>
struct Argument {
	static const int takes = ARGUMENT_ID;
	static const int index = ind;
	using type = Value;
};

template <int ind, class Value>
using SaveArg = Argument<ind, Value>;